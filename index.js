const restify = require("restify");
const mongoose = require("mongoose");
const config = require("./config");
const rjwt = require('restify-jwt-community')
const errors = require("restify-errors");
var restifyValidation = require('node-restify-validation')
var server = restify.createServer();

// Middleware
server.use(restify.plugins.queryParser());
server.use(restifyValidation.validationPlugin( {
  // Shows errors as an array
  errorsAsArray: false,
  // Not exclude incoming variables not specified in validator rules
  forbidUndefinedVariables: false,
  errorHandler:  errors.InvalidArgumentError
}));
server.use(restify.plugins.bodyParser());

// Protect Routes
server.use(rjwt({secret: config.JWT_SECRET}).unless({path: ['/auth']}))


server.listen(config.PORT, () => {
  mongoose.set("useFindAndModify", false);
  mongoose.connect(
    config.MONGODB_URI
    // { useNewUrlParser: true }
  );
});


const db = mongoose.connection;

db.on("error", err => console.log(err));

db.once("open", () => {
  require("./routes/customers")(server);
  require("./routes/users")(server);
  require("./routes/test")(server);

  console.log(`Server stated on port ${config.PORT}`);
});
