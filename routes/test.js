module.exports = server => {
  server.get({path: '/test/:name', validation: {
    resources: {
      name: { isRequired: true, isIn: ['foo','bar'] }
    },
    queries : {
      status: { isRequired: true, isIn: ['foo','bar'] },
      email: { isRequired: false, isEmail: true },
      age: { isRequired: true, isNatural: true }
    }
  }}, function (req, res, next) {
      res.send(req.params);
      next();
  });
}